import QtQuick 1.1

GridView {
    id: desk
    y: 40

    width: parent.width
    height: parent.height

    cellWidth: parent.width / 3
    cellHeight: parent.height / 4

    anchors.horizontalCenter: parent.horizontalCenter

    BreadIngredients {
        id: breadModel
    }

    model: breadModel

    delegate:
        Bread {
            image: picture
            text: name
        }

    Component {
        id: highlight

        Rectangle {
            width: desk.cellWidth
            height: desk.cellHeight
            color: "lightblue"
        }
    }

    highlight: highlight
    focus: true

}
