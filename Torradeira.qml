import QtQuick 1.1

Rectangle {
    id: screen
    width: 450
    height: 750

    Text {
        id: screenHeader
        y: 30
        text: "Torradeira!"
        font.bold: true
        font.pointSize: 25
        color: "blue"

        anchors.horizontalCenter: screen.horizontalCenter
    }

    Desk {
        anchors.top: screenHeader.bottom

    }
}

