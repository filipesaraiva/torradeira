import QtQuick 1.1

Item {

    id: breadModel

    property alias image: breadImage.source
    property alias text: breadName.text

    width: 150
    height: 150

    Image {
        id: breadImage
        width: 100
        height: 100
        smooth: true
        fillMode: Image.PreserveAspectFit
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: breadName
        font.bold: true
        font.pointSize: 14
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: breadImage.bottom
        anchors.topMargin: 15
    }
}
