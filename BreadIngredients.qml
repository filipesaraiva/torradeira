import QtQuick 1.1

XmlListModel {
    id: breadModel

    source: "distros.xml"
    query: "/distros/distro"

    XmlRole {
        name: "name"
        query: "name/string()"
    }
    XmlRole {
        name: "picture"
        query: "picture/string()"
    }
    XmlRole {
        name: "description"
        query: "description/string()"
    }
    XmlRole {
        name: "url"
        query: "url/string()"
    }
    XmlRole {
        name: "iso_32bits"
        query: "iso_32bits/string()"
    }
    XmlRole {
        name: "iso_64bits"
        query: "iso_64bits/string()"
    }
}
